<?php

namespace Drupal\geo_webform\Plugin\WebformElement;

use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'geo_element' element.
 *
 * @WebformElement(
 *   id = "geo_element",
 *   label = @Translation("Geo Map Element"),
 *   description = @Translation("Provides a Geo map element for getting lat/lon coordinates."),
 *   composite = TRUE,
 *   category = @Translation("Geo"),
 * )
 *
 * @see \Drupal\geo_webform\Element\GeoElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Plugin\WebformElement\WebformCompositeBase
 * @see \Drupal\webform\Annotation\WebformElement
 */
class GeoElement extends WebformCompositeBase {

  /**
   * The Map Library Plugin Manager service.
   *
   * @var \Drupal\geofield_map_ext\MapLibraryPluginManager
   */
  protected static $mapLibraryManager;

  /**
   * Google Map Types Options.
   *
   * @var array
   */
  protected $gMapTypesOptions = [
    'roadmap' => 'Roadmap',
    'satellite' => 'Satellite',
    'hybrid' => 'Hybrid',
    'terrain' => 'Terrain',
  ];

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'map_width' => '100%',
      'map_height' => '450px',
      'map_center_default_lat' => '44.83681',
      'map_center_default_lon' => '-0.585317',
      'map_zoom_start' => '16',
      'map_zoom_min' => '0',
      'map_zoom_max' => '22',
      'map_library' => 'leaflet',
      'map_type_google' => 'roadmap',
      'map_type_leaflet' => 'OpenStreetMap_Mapnik',
      'geo_application_url' => '',
      'geo_api_version' => '',
      'map_geocoder' => '',
      'click_to_find_marker' => FALSE,
      'click_to_place_marker' => FALSE,
      'geolocation' => FALSE,
      'hide_coordinates' => FALSE,
    ] + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['geo_container'] = [
      '#type' => 'container',
      '#access' => TRUE,
    ];

    $form['element']['geo_container']['geo_elements'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Geo Element Map Settings'),
      '#access' => TRUE,
    ];
    $form['element']['geo_container']['geo_elements']['map_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Largeur de la carte'),
      '#description' => $this->t('Largeur de la carte au format CSS (ex: 100%).'),
      '#access' => TRUE,
    ];
    $form['element']['geo_container']['geo_elements']['map_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hauteur de la carte'),
      '#description' => $this->t('Hauteur de la carte au format CSS (ex: 450px).'),
      '#access' => TRUE,
    ];
    $form['element']['geo_container']['geo_elements']['map_center_default_lat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Latitude par défaut'),
      '#description' => $this->t('Latitude par défaut du centre de la carte.'),
      '#access' => TRUE,
    ];
    $form['element']['geo_container']['geo_elements']['map_center_default_lon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Longitude par défaut'),
      '#description' => $this->t('Longitude par défaut du centre de la carte.'),
      '#access' => TRUE,
    ];
    $form['element']['geo_container']['geo_elements']['map_zoom_start'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zoom par défaut'),
      '#description' => $this->t('Niveau de zoom à appliquer par défaut au chargement de la carte.'),
      '#access' => TRUE,
    ];

    $form['element']['geo_container']['geo_elements']['map_library'] = [
      '#type' => 'select',
      '#title' => $this->t('Map Library'),
      '#options' => [
        'gmap' => $this->t('Google Map'),
        'leaflet' => $this->t('Leaflet js'),
      ],
      '#access' => TRUE,
    ];

    $form['element']['geo_container']['geo_elements']['map_type_leaflet'] = [
      '#type' => 'select',
      '#title' => $this->t('Map type'),
      '#default_value' => $this->getDefaultProperty('map_type_leaflet'),
      '#options' => $this->getLeafletTileManager()->getLeafletTilesLayersOptions(),
      '#description' => '',
      '#states' => [
        'visible' => [
          ':input[name="properties[map_library]"]' => ['value' => 'leaflet'],
        ],
      ],
    ];

    /** @var \Drupal\geofield_map_ext\MapLibraryPluginInterface $map_library_plugin */
    foreach (self::getMapLibraryManager()->getDefinitions() as $map_library_plugin_definition) {
      $map_library_plugin = self::getMapLibraryManager()->createInstance($map_library_plugin_definition['id']);
      $map_library_plugin->mapWidgetWebformSettings($form['element']['geo_container']['geo_elements']);
    }

    if (Drupal::service('module_handler')->moduleExists('geocoder')) {
      $providers = Drupal::entityTypeManager()
        ->getStorage('geocoder_provider')
        ->loadMultiple();
      $provider_options = [];
      $provider_options[''] = $this->t('None');
      foreach ($providers as $provider) {
        $provider_options[$provider->id()] = $provider->label();
      }

      $form['element']['geo_container']['geo_elements']['map_geocoder'] = [
        '#type' => 'select',
        '#title' => $this->t('Geocoder'),
        '#description' => $this->t("Permet d'afficher une zone de recherche pour effectuer du géocodage d'adresse."),
        '#options' => $provider_options,
        '#access' => TRUE,
      ];
    }

    $form['element']['geo_container']['geo_elements']['hide_coordinates'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide coordinates fields'),
      '#description' => $this->t('Permet de masquer les champs latitude et longitude sur le formulaire.'),
      '#access' => TRUE,
    ];

    $form['element']['geo_container']['geo_elements']['click_to_find_marker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Find marker button'),
      '#description' => $this->t('Affiche un bouton permettant de recentrer la carte sur le marqueur courant.'),
      '#access' => TRUE,
    ];

    $form['element']['geo_container']['geo_elements']['click_to_place_marker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place marker button'),
      '#description' => $this->t('Affiche un bouton permettant de positionner le marqueur au centre de la carte.'),
      '#access' => TRUE,
    ];

    $form['element']['geo_container']['geo_elements']['geolocation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Geolocation button (https only)'),
      '#description' => $this->t("Affiche un bouton permettant à l'utilisateur de centrer la carte sur sa position actuelle.  Attention: l'API HTML5 de géolocalisation requiert https."),
      '#access' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);

    $lines = [];
    $lines[] = ($value['lat'] ? $value['lat'] : '') .
      ($value['lon'] ? ', ' . $value['lon'] : '');
    if (isset($value['geocode'])) {
      $lines[] = $value['geocode'];
    }
    return $lines;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    if (!isset($element['#default_value']['lat'])) {
      if (isset($element['#map_center_default_lat'])) {
        $element['#default_value']['lat'] = $element['#map_center_default_lat'];
      }
      else {
        $element['#default_value']['lat'] = $this->getDefaultProperty('map_center_default_lat');
      }
    }
    if (!isset($element['#default_value']['lon'])) {
      if (isset($element['#map_center_default_lon'])) {
        $element['#default_value']['lon'] = $element['#map_center_default_lon'];
      }
      else {
        $element['#default_value']['lon'] = $this->getDefaultProperty('map_center_default_lon');
      }
    }
    if (!isset($element['#map_dimensions']['width'])) {
      if (isset($element['#map_width'])) {
        $element['#map_dimensions']['width'] = $element['#map_width'];
      }
      else {
        $element['#map_dimensions']['width'] = $this->getDefaultProperty('map_width');
      }
    }
    if (!isset($element['#map_dimensions']['height'])) {
      if (isset($element['#map_height'])) {
        $element['#map_dimensions']['height'] = $element['#map_height'];
      }
      else {
        $element['#map_dimensions']['height'] = $this->getDefaultProperty('map_height');
      }
    }
    if (!isset($element['#zoom']['start'])) {
      if (isset($element['#map_zoom_start'])) {
        $element['#zoom']['start'] = $element['#map_zoom_start'];
      }
      else {
        $element['#zoom']['start'] = $this->getDefaultProperty('map_zoom_start');
      }
    }
    if (!isset($element['#zoom']['focus'])) {
      if (isset($element['#map_zoom_start'])) {
        $element['#zoom']['focus'] = $element['#map_zoom_start'];
      }
      else {
        $element['#zoom']['focus'] = $this->getDefaultProperty('map_zoom_start');
      }
    }
    if (!isset($element['#zoom']['min'])) {
      $element['#zoom']['min'] = $this->getDefaultProperty('map_zoom_min');
    }
    if (!isset($element['#zoom']['max'])) {
      $element['#zoom']['max'] = $this->getDefaultProperty('map_zoom_max');
    }

    if (!empty($element['#map_geocoder'])) {
      $element['#gmap_geocoder'] = 1;
      $element['#gmap_geocoder_settings'] = [
        'providers' => [$element['#map_geocoder']],
        'min_terms' => '4',
        'delay' => '800',
        'zoom' => '16',
        'options' => [
          'options' => [],
        ],
      ];
    }

    if (!isset($element['#map_library'])) {
      $element['#map_library'] = $this->getDefaultProperty('map_library');
    }
    if ($element['#map_library'] === 'leaflet') {
      if (!isset($element['#map_type_leaflet'])) {
        $element['#map_type'] = $this->getDefaultProperty('map_type_leaflet');
      }
      else {
        $element['#map_type'] = $element['#map_type_leaflet'];
      }
    }
    if ($element['#map_library'] === 'gmap') {
      $element['#gmap_api_key'] = $this->getGmapApiKey();
      $element['#map_type'] = $this->getDefaultProperty('map_type_google');
    }

    $element['#map_types_google'] = $this->gMapTypesOptions;
    $element['#map_types_leaflet'] = $this->getLeafletTileManager()->getLeafletTileLayers();

    if (self::getMapLibraryManager()->hasDefinition($element['#map_library'])) {
      /** @var \Drupal\geofield_map_ext\MapLibraryPluginInterface $map_library_plugin */
      $map_library_plugin = self::getMapLibraryManager()->createInstance($element['#map_library']);
      if (isset($map_library_plugin)) {
        $map_library_plugin->mapWidgetWebformElement($element);
      }
    }
  }

  /**
   * @return object|null
   */
  protected function getLeafletTileManager() {
    return Drupal::service('plugin.manager.leaflet_tile_layer_plugin');
  }

  /**
   * Get the GMap Api Key from the geofield_map.google_maps service.
   *
   * @return string
   *   The GMap Api Key
   */
  protected function getGmapApiKey() {
    return Drupal::service('geofield_map.google_maps')->getGmapApiKey();
  }

  /**
   * Fetchs the Map Library Plugin Manager service.
   * @return \Drupal\geofield_map_ext\MapLibraryPluginManager|mixed
   */
  protected static function getMapLibraryManager() {
    if (empty(self::$mapLibraryManager)) {
      self::$mapLibraryManager = Drupal::service('plugin.manager.map_library_plugin');
    }
    return self::$mapLibraryManager;
  }

}
